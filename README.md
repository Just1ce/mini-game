# Mini game

[![pipeline status](https://gitlab.com/Just1ce/mini-game/badges/master/pipeline.svg)](https://gitlab.com/Just1ce/mini-game/commits/master)
[![pipeline status](https://gitlab.com/Just1ce/mini-game/badges/master/coverage.svg)](https://gitlab.com/Just1ce/mini-game/commits/master)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg?style=flat-square)](https://conventionalcommits.org)
![npm type definitions](https://img.shields.io/npm/types/typescript)

Easy for scale, maintenance and releases

## How to start application

To run production version:

1. `cp ./.env.default ./.env`
2. Change DB credentials for your wishes
3. `docker-compose up -d`

To run development mode:
- `cp ./.env.default ./.env`
- Install all dependencies:
```bash
cd ./game-api && npm i && cd ../websocket && npm i && cd ../frontend && npm i && cd ../
``` 
- Launch infrastructure and backend API: 
```bash
docker-compose -f docker-compose.dev.yml up -d
```
- Launch frontend in development mode:
```bash
cd ./frontend && npm start
```

p.s All migrations will run in container in production mode and development as well

Hooray that's all you need to run the application. Now you can open a browser in http://localhost

## Disclaimer

Basically fronted/game-api/websocket and of course reverse proxy should be placed in separated repositories. This is just example to simplify demo.

## Tests

e2e and unit tests was developed for game API.
To run unit tests:

```bash
cd ./game-api && npm run test:cov
```

To run e2e tests:

```bash
cd ./game-api && npm run test:e2e
```

### What next

- Here should be blue-green deploy with upstreams in nginx
- CD with gitlab CI
- add semantic release for versions
