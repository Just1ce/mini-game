import { MigrationInterface, QueryRunner } from 'typeorm';

export class Room1569357435128 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TYPE "room_color_enum" AS ENUM('0', '1', '2')`,
      undefined,
    );
    await queryRunner.query(
      `CREATE TABLE "rooms" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "color" "room_color_enum" NOT NULL DEFAULT '0', CONSTRAINT "PK_c6d46db005d623e691b2fbcba23" PRIMARY KEY ("id"))`,
      undefined,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE "rooms"`, undefined);
    await queryRunner.query(`DROP TYPE "room_color_enum"`, undefined);
  }
}
