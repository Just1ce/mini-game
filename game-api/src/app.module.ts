import { Module, ValidationPipe } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_PIPE } from '@nestjs/core';
import { ConfigModule } from './config/config.module';
import { ApplicationConfigToken } from './config/constants';
import { ApplicationConfigInterface } from './config/application-config.interface';
import { GameRoomModule } from './game-room/game-room.module';

@Module({
  imports: [
    GameRoomModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ApplicationConfigInterface) => ({
        type: 'postgres',
        host: config.postgresHost,
        port: config.postgresPort,
        username: config.postgresUsername,
        password: config.postgresPassword,
        database: config.postgresDatabase,
        entities: [`${__dirname}/**/*.entity.{ts,js}`],
        migrations: [`${__dirname}/migrations/*.{ts,js}`],
      }),
      inject: [ApplicationConfigToken],
    }),
  ],
  controllers: [],
  providers: [
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
  ],
})
export class AppModule {}
