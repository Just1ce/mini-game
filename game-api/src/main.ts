import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { ApplicationConfigInterface } from './config/application-config.interface';
import { ApplicationConfigToken } from './config/constants';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const config = app.get<ApplicationConfigInterface>(ApplicationConfigToken);
  await app.listen(config.httpPort);
}
bootstrap();
