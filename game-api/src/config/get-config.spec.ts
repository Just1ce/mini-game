import { IsString } from 'class-validator';
import { Expose } from 'class-transformer';
import { getConfig } from './get-config';

class TestConfig {
  @IsString()
  @Expose({ name: 'ID' })
  id: string;
}

describe('Configuration', () => {
  it('Should throw error', () => {
    expect.assertions(1);

    try {
      getConfig({}, TestConfig);
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
    }
  });

  it('Should transform from .env format', () => {
    const config = getConfig(
      {
        ID: 'test',
      },
      TestConfig,
    );

    expect(config.id).toBe('test');
  });
});
