export interface ApplicationConfigInterface {
  readonly redisPort: number;
  readonly redisHost: string;
  readonly httpPort: number;

  readonly postgresPort: number;
  readonly postgresHost: string;
  readonly postgresUsername: string;
  readonly postgresPassword: string;
  readonly postgresDatabase: string;
}
