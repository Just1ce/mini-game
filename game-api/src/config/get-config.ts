import { plainToClass } from 'class-transformer';
import { validateSync } from 'class-validator';

export declare type ClassType<T> = {
  new (...args: any[]): T;
};
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const getConfig = <T>(
  rawConfigObject: any,
  ApplicationConfigClass: ClassType<T>,
): T => {
  const applicationConfig = plainToClass(
    ApplicationConfigClass,
    rawConfigObject,
  );
  const validationErrors = validateSync(applicationConfig);
  if (validationErrors.length > 0) {
    const error = new Error('Application config validation');
    console.error(validationErrors);
    // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
    // @ts-ignore
    error.validationErrors = validationErrors;
    throw error;
  }

  return applicationConfig;
};
