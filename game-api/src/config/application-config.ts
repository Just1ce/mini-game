import { IsPort, IsString } from 'class-validator';
import { Expose } from 'class-transformer';
import { ApplicationConfigInterface } from './application-config.interface';

export class ApplicationConfig implements ApplicationConfigInterface {
  @Expose({ name: 'REDIS_PORT' })
  @IsPort()
  public readonly redisPort: number;

  @Expose({ name: 'REDIS_HOST' })
  @IsString()
  readonly redisHost: string;

  @Expose({ name: 'HTTP_PORT' })
  @IsPort()
  public readonly httpPort: number;

  @Expose({ name: 'PG_DB_NAME' })
  readonly postgresDatabase: string;

  @Expose({ name: 'PG_HOST' })
  readonly postgresHost: string;

  @Expose({ name: 'PG_PASSWORD' })
  readonly postgresPassword: string;

  @Expose({ name: 'PG_PORT' })
  readonly postgresPort: number;

  @Expose({ name: 'PG_USER' })
  readonly postgresUsername: string;
}
