import { Module } from '@nestjs/common';
import { ApplicationConfigToken } from './constants';
import { getConfig } from './get-config';
import { ApplicationConfig } from './application-config';

@Module({
  imports: [],
  controllers: [],
  providers: [
    {
      provide: ApplicationConfigToken,
      useFactory: () => {
        return getConfig(process.env, ApplicationConfig);
      },
    },
  ],
  exports: [ApplicationConfigToken],
})
export class ConfigModule {}
