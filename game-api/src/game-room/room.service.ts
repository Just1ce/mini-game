import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RoomServiceInterface } from './room-service.interface';
import { Colors, Room } from './room.entity';
import { SocketEmitter } from './socket-emitter';
import { socketEmitterInterfaceToken } from './constants';

const nextColorsMapper = {
  [Colors.BLUE]: Colors.GREEN,
  [Colors.GREEN]: Colors.RED,
  [Colors.RED]: Colors.BLUE,
};

@Injectable()
export class RoomService implements RoomServiceInterface {
  constructor(
    @InjectRepository(Room) private readonly roomRepository: Repository<Room>,
    @Inject(socketEmitterInterfaceToken)
    private readonly socketEmitter: SocketEmitter,
  ) {}

  async create(name: string): Promise<Room> {
    const newRoom = this.roomRepository.create({ name });
    await this.roomRepository.save(newRoom);
    this.socketEmitter.emitRoomCreation(newRoom);
    return newRoom;
  }

  getAll(): Promise<Array<Room>> {
    return this.roomRepository.find({ order: { id: 'ASC' } });
  }

  getById(id: number): Promise<Room | null> {
    return this.roomRepository.findOne(id);
  }

  async changeStateColor(id: number): Promise<void> {
    const room = await this.roomRepository.findOne(id);
    if (!room) {
      return;
    }

    room.color = nextColorsMapper[room.color];
    await this.roomRepository.save(room);
    this.socketEmitter.emitRoomChangeColor(room.id, room.color);
  }
}
