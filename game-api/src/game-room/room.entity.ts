import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

export enum Colors {
  GREEN,
  RED,
  BLUE,
}

@Entity({ name: 'rooms' })
export class Room {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'enum', enum: Colors, default: Colors.GREEN })
  color: Colors;
}
