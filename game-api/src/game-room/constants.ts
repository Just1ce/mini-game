export const roomServiceInterfaceToken = 'RoomServiceInterface';
export const socketEmitterInterfaceToken = 'SocketEmitterInterface';
export const socketEmitterAdapterToken = 'SocketEmitterAdapterToken';
