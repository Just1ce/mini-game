import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Colors, Room } from './room.entity';
import { RoomService } from './room.service';
import { RoomServiceInterface } from './room-service.interface';
import { socketEmitterInterfaceToken } from './constants';

describe('RoomService', () => {
  let ctx: {
    roomRepositoryMock;
    socketEmitterMock;
    roomService: RoomServiceInterface;
    [key: string]: any;
  };

  beforeEach(async () => {
    const roomRepositoryMock = {
      save: jest.fn(),
      create: jest.fn(),
      findOne: jest.fn(),
    };

    const socketEmitterMock = {
      emitRoomChangeColor: jest.fn(),
      emitRoomCreation: jest.fn(),
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RoomService,
        {
          provide: getRepositoryToken(Room),
          useValue: roomRepositoryMock,
        },
        {
          provide: socketEmitterInterfaceToken,
          useValue: socketEmitterMock,
        },
      ],
    }).compile();

    ctx = {
      roomRepositoryMock,
      socketEmitterMock,
      roomService: module.get<RoomServiceInterface>(RoomService),
    };
  });

  describe('Room creation', () => {
    beforeEach(async () => {
      const roomName = 'test';
      const room = new Room();
      room.name = roomName;
      ctx.roomRepositoryMock.create.mockReturnValueOnce(room);
      ctx.expectedRoomName = roomName;
      ctx.resultRoom = await ctx.roomService.create(roomName);
    });

    it('result should be instance of Room', () => {
      expect(ctx.resultRoom).toBeInstanceOf(Room);
    });

    it('Should create room with given name', () => {
      expect(ctx.resultRoom.name).toBe(ctx.expectedRoomName);
    });

    it('Should has default state', () => {
      expect(ctx.resultRoom.color).not.toBeNull();
    });

    it('Should emit event', () => {
      expect(ctx.socketEmitterMock.emitRoomCreation).toBeCalledWith(
        ctx.resultRoom,
      );
    });
  });

  describe('Room change color', () => {
    beforeEach(async () => {
      ctx.roomId = 10;
      ctx.room = new Room();
      ctx.room.id = ctx.roomId;
      ctx.room.color = Colors.BLUE;

      ctx.roomRepositoryMock.findOne.mockReturnValue(ctx.room);
    });

    it('Should emit event on change color if room exists', async () => {
      await ctx.roomService.changeStateColor(ctx.roomId);
      expect(ctx.socketEmitterMock.emitRoomChangeColor).toBeCalledWith(
        ctx.roomId,
        ctx.room.color,
      );
    });

    it('Should call save', async () => {
      await ctx.roomService.changeStateColor(ctx.roomNumber);
      expect(ctx.roomRepositoryMock.save).toBeCalledWith(ctx.room);
    });

    it('Should not emit event if there is no room', async () => {
      ctx.roomRepositoryMock.findOne.mockReturnValue(null);
      await ctx.roomService.changeStateColor(10);
      expect(ctx.socketEmitterMock.emitRoomChangeColor).not.toBeCalled();
    });
  });
});
