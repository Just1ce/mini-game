import { Colors, Room } from './room.entity';

export interface SocketEmitterInterface {
  emitRoomCreation(room: Room);
  emitRoomChangeColor(roomId: number, color: Colors);
}
