import { Room } from './room.entity';

export interface RoomServiceInterface {
  create(name: string): Promise<Room>;
  getAll(): Promise<Array<Room>>;
  getById(id: number): Promise<Room>;
  changeStateColor(id: number): Promise<void>;
}
