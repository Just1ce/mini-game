import {
  Body,
  Controller,
  Get,
  Inject,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { RoomCreationDto } from './room-creation.dto';
import { roomServiceInterfaceToken } from './constants';
import { RoomServiceInterface } from './room-service.interface';

@Controller('/api/rooms')
export class RoomController {
  constructor(
    @Inject(roomServiceInterfaceToken)
    private readonly roomService: RoomServiceInterface,
  ) {}

  @Post('')
  public async createRoom(@Body() roomCreationDto: RoomCreationDto) {
    return this.roomService.create(roomCreationDto.name);
  }

  @Get('')
  public async getAllRooms() {
    return this.roomService.getAll();
  }

  @Get(':id')
  public async getRoom(@Param('id', ParseIntPipe) id: number) {
    const room = await this.roomService.getById(id);
    if (!room) {
      throw new NotFoundException();
    }

    return room;
  }

  @Put(':id')
  public async changeColor(@Param('id', ParseIntPipe) id: number) {
    await this.roomService.changeStateColor(id);
  }
}
