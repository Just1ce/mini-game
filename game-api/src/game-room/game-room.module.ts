import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import ioEmitter from 'socket.io-emitter';
import { Room } from './room.entity';
import {
  roomServiceInterfaceToken,
  socketEmitterAdapterToken,
  socketEmitterInterfaceToken,
} from './constants';
import { RoomService } from './room.service';
import { SocketEmitter } from './socket-emitter';
import { ApplicationConfigToken } from '../config/constants';
import { ApplicationConfigInterface } from '../config/application-config.interface';
import { RoomController } from './room.controller';
import { ConfigModule } from '../config/config.module';

@Module({
  imports: [ConfigModule, TypeOrmModule.forFeature([Room])],
  controllers: [RoomController],
  providers: [
    {
      provide: roomServiceInterfaceToken,
      useClass: RoomService,
    },
    {
      provide: socketEmitterInterfaceToken,
      useClass: SocketEmitter,
    },
    {
      provide: socketEmitterAdapterToken,
      useFactory: (config: ApplicationConfigInterface) => {
        return ioEmitter({ host: config.redisHost, port: config.redisPort });
      },
      inject: [ApplicationConfigToken],
    },
  ],
})
export class GameRoomModule {}
