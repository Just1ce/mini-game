import { Inject, Injectable } from '@nestjs/common';
import { SocketEmitterInterface } from './socket-emitter.interface';
import { Colors, Room } from './room.entity';
import { socketEmitterAdapterToken } from './constants';

const roomPrefix = 'game-room:';

@Injectable()
export class SocketEmitter implements SocketEmitterInterface {
  constructor(
    @Inject(socketEmitterAdapterToken) private readonly emitterAdapter,
  ) {}

  emitRoomChangeColor(roomId: number, color: Colors) {
    this.emitterAdapter
      .to(`${roomPrefix}${roomId}`)
      .broadcast.emit('changeColor', { color });
  }

  emitRoomCreation(room: Room) {
    this.emitterAdapter.broadcast.emit('roomCreation', {
      ...room,
    });
  }
}
