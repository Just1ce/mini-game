import { IsString } from 'class-validator';

export class RoomCreationDto {
  @IsString()
  name: string;
}
