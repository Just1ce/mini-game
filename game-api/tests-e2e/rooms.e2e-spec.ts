import { INestApplication } from '@nestjs/common';
import supertest from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../src/app.module';

describe('Rooms API', () => {
  let ctx: {
    http: supertest.SuperTest<supertest.Test>;
    app: INestApplication;
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    const app = moduleFixture.createNestApplication();
    await app.init();

    ctx = {
      app,
      http: supertest(app.getHttpServer()),
    };
  });

  afterEach(async () => {
    await ctx.app.close();
  });

  // In development u have to erase ur test DB
  it('Should return empty array of rooms', async () => {
    const { body } = await ctx.http.get(`/api/rooms`).expect(200);

    expect(body).toHaveLength(0);
  });

  it('Should create room', async () => {
    const { body } = await ctx.http
      .post('/api/rooms')
      .send({ name: 'testRoom' })
      .expect(201);

    const { body: getBody } = await ctx.http
      .get(`/api/rooms/${body.id}`)
      .expect(200);

    expect(getBody.id).toBe(body.id);
  });

  it('Should return not empty rooms', async () => {
    const { body } = await ctx.http.get(`/api/rooms`).expect(200);

    expect(body).toHaveLength(1);
  });

  it('Should change color', async () => {
    const { body: newRoom } = await ctx.http
      .post('/api/rooms')
      .send({ name: 'testRoom' })
      .expect(201);

    await ctx.http.put(`/api/rooms/${newRoom.id}`).expect(200);

    const { body: updatedRoom } = await ctx.http
      .get(`/api/rooms/${newRoom.id}`)
      .expect(200);

    expect(updatedRoom.color).not.toBe(newRoom.color);
  });

  it('Should return 404 if there is no such room', async () => {
    await ctx.http.get(`/api/rooms/12312312`).expect(404);
  });
});
