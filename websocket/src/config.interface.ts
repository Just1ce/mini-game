export interface ConfigInterface {
  wsPort: number;
  redisPort: number;
  redisHost: string;
}
