import socketIo, { Socket } from 'socket.io';
import redisAdapter from 'socket.io-redis';
import { ConfigInterface } from './config.interface';
import { Config } from './config';
import { getConfig } from './get-config';

const rawConfig = process.env;
const config: ConfigInterface = getConfig(rawConfig, Config);

const wsServer = socketIo({
  transports: ['websocket'],
  pingInterval: 10000,
}).adapter(redisAdapter({ host: config.redisHost, port: config.redisPort }));

wsServer.on('connection', (socket: Socket) => {
  socket.on('roomJoin', ({ roomId }) => {
    socket.join(`game-room:${roomId}`);
  });
});

wsServer.listen(config.wsPort);
console.log('listen', config.wsPort);
