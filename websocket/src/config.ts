import { IsPort, IsString } from 'class-validator';
import { Expose } from 'class-transformer';
import { ConfigInterface } from './config.interface';

export class Config implements ConfigInterface {
  @Expose({ name: 'REDIS_PORT' })
  @IsPort()
  public readonly redisPort: number;

  @Expose({ name: 'REDIS_HOST' })
  @IsString()
  readonly redisHost: string;

  @Expose({ name: 'WS_PORT' })
  @IsPort()
  public readonly wsPort: number;
}
