import React from 'react';
import ReactDOM from 'react-dom';
import createSocketIO from 'socket.io-client';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

const socket = createSocketIO('http://localhost:3000', {
  transports: ['websocket'],
});

fetch('/api/rooms')
  .then(response => {
    return response.json();
  })
  .then(data => {
    ReactDOM.render(
      <App socket={socket} rooms={data} />,
      document.getElementById('root'),
    );
  })
  .catch(error => {
    alert(error.message);
  });

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
