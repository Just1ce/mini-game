import React, { useEffect, useState } from 'react';
import './App.css';
import { RoomList } from './components/RoomList';
import { Room } from './components/Room';

const App = ({ socket, rooms: initialRooms }) => {
  const [rooms, updateRooms] = useState(initialRooms);
  const [showListRooms, setShowListRooms] = useState(true);
  const [currentRoom, updateCurrentRoom] = useState({});

  useEffect(() => {
    socket.on('roomCreation', room => {
      updateRooms([...rooms, room]);
    });

    socket.on('changeColor', ({ color }) => {
      updateCurrentRoom({ ...currentRoom, color });
    });

    socket.on('reconnect', () => {
      if (currentRoom.id) {
        socket.emit('roomJoin', { roomId: currentRoom.id });
      }
    });

    return () => {
      socket.off('changeColor');
      socket.off('roomCreation');
    };
  });

  const onClickChangeColor = id => {
    return fetch(`/api/rooms/${id}`, {
      method: 'PUT',
    });
  };

  const onClickRoomEnter = roomId => {
    fetch(`/api/rooms/${roomId}`)
      .then(response => response.json())
      .then(room => {
        socket.emit('roomJoin', { roomId });
        updateCurrentRoom(room);
        setShowListRooms(false);
      });
  };

  const onRoomAdd = name => {
    return fetch('/api/rooms', {
      method: 'POST',
      body: JSON.stringify({ name }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
  };

  const onClickComeback = () => {
    setShowListRooms(true);
    updateCurrentRoom({});
  };

  return (
    <div className="App">
      {showListRooms ? (
        <RoomList
          rooms={rooms}
          onClickRoomEnter={onClickRoomEnter}
          onRoomAdd={onRoomAdd}
        />
      ) : (
        <Room
          room={currentRoom}
          onChangeColor={onClickChangeColor}
          onClickComeback={onClickComeback}
        />
      )}
    </div>
  );
};

export default App;
