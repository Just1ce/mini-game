import React from 'react';

const colorsMap = {
  0: 'green',
  1: 'red',
  2: 'blue',
};

export const Room = ({ room, onChangeColor, onClickComeback }) => {
  const color = colorsMap[room.color];

  const onChangeColorWithId = () => {
    onChangeColor(room.id);
  };

  return (
    <div className="body">
      <h3>{room.name}</h3>
      <div>
        <button type="button" onClick={onClickComeback}>
          Back to list
        </button>
      </div>
      <div
        className="shape"
        style={{ backgroundColor: color }}
        onClick={onChangeColorWithId}
      />
    </div>
  );
};
