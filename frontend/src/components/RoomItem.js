import React from 'react';

export const RoomItem = ({ room, onClickRoom }) => {
  const onEnter = () => {
    onClickRoom(room.id);
  };

  return (
    <div className="room-item">
      <span onClick={onEnter}>
        {room.id}:{room.name}
      </span>
    </div>
  );
};
