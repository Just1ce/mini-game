import React, { useState } from 'react';
import { RoomItem } from './RoomItem';

export const RoomList = ({ rooms, onClickRoomEnter, onRoomAdd }) => {
  const [roomName, setRoomName] = useState('');

  const onRoomAddWithData = () => {
    onRoomAdd(roomName);
  };

  const onChangeRoomNameInput = ({ target: { value } }) => {
    setRoomName(value);
  };

  return (
    <div>
      <div>
        {rooms.map(room => {
          return (
            <RoomItem
              key={room.id}
              room={room}
              onClickRoom={onClickRoomEnter}
            />
          );
        })}
      </div>
      <div>
        <input type="text" value={roomName} onChange={onChangeRoomNameInput} />
        <button type="button" onClick={onRoomAddWithData}>
          Add room
        </button>
      </div>
    </div>
  );
};
